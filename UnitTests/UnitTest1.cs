using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sphinx;

namespace UnitTests
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			String test = "-hello -hello\\ world -\"hello, my \\\"great\\\" world";
			SphinxParser parser = new SphinxParser();

			SphinxParsedQuery query = parser.Parse( test );
			query.ToString();
		}
	}
}
