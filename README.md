# README #
**Sphinx** is meant to be a standalone advanced human-readable query parsing library that will work with any dataset. The ultimate goal is to create a library that can be dropped in with minimal effort into any C# or .Net application that has a search box and be able to have that search box provide advanced and refined searches.

Sphinx does not actually perform the dataset searching itself. Rather, Sphinx is designed to take a raw string that contains a search and extract every possible piece of information about it and return it back in an easy-to-consume data structure that is infinitely more manageable for a programmer to use. Trying to allow Sphinx to do the data searching itself would be pointless; it would require some form of enforcing of the data structure to be searched through, which would ruin the purpose of having this as a library and not a framework. Not to mention, that is always best left to the individual(s) who best know their own datasets and the requirements and technical limitations of their application. Rather, Sphinx will take a string `(c# OR .Net) AND library AND "query parsing" AND "dynamic \"string\" interpretation -VB -Basic -Databases` and give back a structure that says that:

* The result should contain the strings 'c#' or '.Net'
* The result needs to match the above criteria AND contain the string "library" and the EXACT matches 'query parsing' and 'dynamic "string" interpretation"
* The result needs to match the above criteria and not contain the strings 'VB', 'Basic', or 'Databases'

Sphinx is, of course, far more feature-rich than just this simple of an advanced query, and a full feature-set will be documented as the project advances.


### Design Guidelines ###
The following are the core principles that Sphinx is designed with in mind.

* **Threadsafe.** Multiple threads should be able to call the main `Parse` functions at the same time without worry of corrupting any internal states. This, however, is only guaranteed with multiple calls to `Parse` functions. Calls to functions to configure *how* queries are parsed (such as turning on or off feature detection) do not have the same threadsafe guarantee.
* **Query parsing only.** There will be no final interaction with the datasets, and the return data will always be given in easy-to-use structures and manners, but in a generic and uniform way rather than the return structure being dictated based on a particular dataset.
* **Open source.**
* **No Regex.**
* **Forward String Iteration.** When parsing a single string, we will only iterate over that string once. States should be preserved between character to character to provide a history of where we've come, rather than needing to look back at previous characters in the input string.

### How do I get set up? ###

Sphinx is written in C# 7.0, with the development environment being Microsoft Visual Studio 2017. There are no external dependencies that need to be configured separately in order to get Sphinx to compile; however, there *are* some NuGet packages used by Sphinx:

* System.Collections.Immutable by Microsoft, v1.3.1