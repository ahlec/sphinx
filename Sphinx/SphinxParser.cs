﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sphinx.Internal;
using Sphinx.Utils;

namespace Sphinx
{
	public sealed class SphinxParser
	{
		public SphinxParsedQuery Parse( String query )
		{
			SphinxParsedQuery parsed;
			ParseOperationError error = PerformParse( query, out parsed );
			if ( error != null )
			{
				throw new ApplicationException( error.ErrorMessage );
			}
			return parsed;
		}

		public Boolean TryParse( String query, out SphinxParsedQuery parsed )
		{
			ParseOperationError error = PerformParse( query, out parsed );
			if ( error != null )
			{
				parsed = null;
				return false;
			}
			return true;
		}

		ParseOperationError PerformParse( String query, out SphinxParsedQuery parsed )
		{
			parsed = new SphinxParsedQuery( query );
			if ( query == null || query.Length == 0 )
			{
				return null;
			}

			ParseOperationError error = null;
			SphinxKeywordCollector keywordCollector = new SphinxKeywordCollector();
			SphinxQueryStringBuilder queryStringBuilder = new SphinxQueryStringBuilder( parsed );
			Int32 indexOfEscapeCharacter = -1;
			Boolean isInsideQuotationsMarks = false;
			for ( Int32 index = 0; index < query.Length; ++index )
			{
				if ( error != null )
				{
					return error;
				}

				Char character = query[index];

				if ( character == CharUtils.EscapeCharacter && indexOfEscapeCharacter < 0 )
				{
					indexOfEscapeCharacter = index;
					continue;
				}
				else if ( indexOfEscapeCharacter >= 0 && index - indexOfEscapeCharacter >= 2 )
				{
					indexOfEscapeCharacter = -1;
				}

				if ( isInsideQuotationsMarks )
				{
					if ( CharUtils.IsClosingQuotationMark( character ) && indexOfEscapeCharacter < 0 )
					{
						isInsideQuotationsMarks = false;
						error = queryStringBuilder.PublishToCurrentDestination();
						continue;
					}

					error = queryStringBuilder.Append( character );
					continue;
				}

				if ( queryStringBuilder.IsBuilding )
				{
					if ( CharUtils.IsOpeningQuotationMark( character ) && indexOfEscapeCharacter < 0 && queryStringBuilder.CanModifyMetadata )
					{
						error = queryStringBuilder.MakeRequiresExactMatch();
						isInsideQuotationsMarks = true;
						continue;
					}

					if ( Char.IsWhiteSpace( character ) && indexOfEscapeCharacter < 0 )
					{
						error = queryStringBuilder.PublishToCurrentDestination();
						continue;
					}

					error = queryStringBuilder.Append( character );
					continue;
				}

				if ( character == CharUtils.ExcludedStringPrefix )
				{
					error = queryStringBuilder.Start( SphinxQueryStringDestination.ExcludedStrings );
					continue;
				}

				SphinxKeyword keyword = keywordCollector.Add( character );
				if ( keyword != SphinxKeyword.None )
				{
					switch ( keyword )
					{
						case SphinxKeyword.And:
							{
								break;
							}
						case SphinxKeyword.Or:
							{
								break;
							}
						default:
							{
								error = new ParseOperationError( $"{keyword} is not implemented yet." );
								break;
							}
					}
					continue;
				}

				if ( Char.IsWhiteSpace( character ) )
				{
					keywordCollector.Reset();
					continue;
				}
			}

			// Clean up any outstanding data
			queryStringBuilder.PublishToCurrentDestination();

			return null;
		}
	}
}
