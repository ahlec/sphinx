﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx.Internal
{
	internal class SphinxKeywordCollector
	{
		class KeywordInfo
		{
			public KeywordInfo( SphinxKeyword keyword )
			{
				Keyword = keyword;
			}

			public readonly SphinxKeyword Keyword;
			public Boolean IsValid;
		}

		internal SphinxKeywordCollector()
		{
			Reset();
		}

		/// <returns>Returns the keyword that was just built by the addition of this character, otherwise <see cref="SphinxKeyword.None"/>.</returns>
		public SphinxKeyword Add( Char character )
		{
			if ( Char.IsLower( character ) )
			{
				character = Char.ToUpper( character );
			}
			else if ( !Char.IsUpper( character ) )
			{
				MarkAllOperators( false );
				return SphinxKeyword.None;
			}

			Int32 numOperatorsStillValid = 0;
			foreach ( String operatorString in _operators.Keys )
			{
				if ( !_operators[operatorString].IsValid )
				{
					continue;
				}

				if ( _currentIndex >= operatorString.Length )
				{
					_operators[operatorString].IsValid = false;
					continue;
				}

				if ( operatorString[_currentIndex] != character )
				{
					_operators[operatorString].IsValid = false;
				}

				if ( operatorString.Length == _currentIndex + 1 )
				{
					MarkAllOperators( false, operatorString );
					return _operators[operatorString].Keyword;
				}

				++numOperatorsStillValid;
			}

			++_currentIndex;
			return SphinxKeyword.None;
		}

		public void Reset()
		{
			MarkAllOperators( true );
			_currentIndex = 0;
		}

		void MarkAllOperators( Boolean valid, String exception = null )
		{
			foreach ( String operatorString in _operators.Keys )
			{
				_operators[operatorString].IsValid = ( exception == operatorString ? !valid : valid );
			}
		}

		/// <summary>
		/// Operator String --> if it is still valid to be considered given the characters that came immediately before it
		/// <para />
		/// All operators are in upper case
		/// </summary>
		readonly Dictionary<String, KeywordInfo> _operators = new Dictionary<String, KeywordInfo>
		{
			{ "AND",	new KeywordInfo( SphinxKeyword.And ) },
			{ "OR",		new KeywordInfo( SphinxKeyword.Or ) }
		};
		Int32 _currentIndex = 0;
	}
}
