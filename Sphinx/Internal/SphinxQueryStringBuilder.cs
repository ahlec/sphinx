﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx.Internal
{
	internal sealed class SphinxQueryStringBuilder
	{
		internal SphinxQueryStringBuilder( SphinxParsedQuery query )
		{
			_query = query;
			_stringBuilder = new StringBuilder();
		}

		internal Boolean IsBuilding => ( _currentDestination != null );

		internal Boolean CanModifyMetadata => ( IsBuilding && _stringBuilder.Length == 0 );

		internal ParseOperationError Start( List<SphinxQueryString> destination )
		{
			if ( _currentDestination != null )
			{
				return new ParseOperationError( $"The {nameof( SphinxQueryStringBuilder )} is already in the destination state of {_currentDestination}" );
			}
			if ( destination == null )
			{
				return new ParseOperationError( $"{destination} is not a valid destination" );
			}

			_currentDestination = destination;
			_requiresExactMatch = false;
			_stringBuilder.Clear();

			return null;
		}

		internal ParseOperationError MakeRequiresExactMatch()
		{
			if ( _currentDestination == null )
			{
				return new ParseOperationError( $"{_currentDestination} is not a valid destination" );
			}
			if ( !CanModifyMetadata )
			{
				return new ParseOperationError( "The string buffer has already begun to be modified. Metadata cannot be modified anymore." );
			}

			_requiresExactMatch = true;
			return null;
		}

		internal ParseOperationError Append( Char character )
		{
			if ( _currentDestination == null )
			{
				return new ParseOperationError( $"{nameof( _currentDestination )} is in an invalid state ({_currentDestination})" );
			}

			_stringBuilder.Append( character );
			return null;
		}

		/// <summary>
		/// Safe to call even when not in a valid state
		/// </summary>
		internal ParseOperationError PublishToCurrentDestination()
		{
			if ( _currentDestination == null )
			{
				_stringBuilder.Clear();
				return null;
			}

			SphinxQueryString payload = new SphinxQueryString
			{
				String = _stringBuilder.ToString(),
				RequiresExactMatch = _requiresExactMatch
			};

			// Reset internal states
			_stringBuilder.Clear();

			// Publish
			_currentDestination.Add( payload );
			_currentDestination = null;

			return null;
		}

		readonly StringBuilder _stringBuilder;
		readonly SphinxParsedQuery _query;
		List<SphinxQueryString> _currentDestination;
		Boolean _requiresExactMatch;
	}
}
