﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx.Internal
{
	/// <summary>
	/// A function that returns <see cref="null"/> instead of an instance of this class means that there was no error encountered in performing that operation.
	/// </summary>
	internal class ParseOperationError
	{
		internal ParseOperationError( String error )
		{
			ErrorMessage = error;
		}

		public String ErrorMessage { get; }
	}
}
