﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx.Internal
{
	internal enum SphinxKeyword
	{
		None = 0x0,

		And,
		Or
	}
}
