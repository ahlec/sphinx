﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx
{
	public enum SphinxConnectiveOperator
	{
		None,
		And,
		Or
	}
}
