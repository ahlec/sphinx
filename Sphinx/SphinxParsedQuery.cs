﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx
{
	public sealed class SphinxParsedQuery
	{
		internal SphinxParsedQuery( String rawQueryText )
		{
			RawQueryText = rawQueryText;
		}

		public SphinxQueryCollection Queries { get; }

		public String RawQueryText { get; }
	}
}
