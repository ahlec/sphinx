﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx
{
	public sealed class SphinxQueryCollection : ISphinxClause, IEnumerable<SphinxQuery>
	{
		/// <summary>
		/// A collection of all of the <see cref="ISphinxClause"/>s that are combined together by the <see cref="Operator"/>.
		/// Each individiual clause can be one of the following:
		/// <para>* Another <see cref="SphinxQueryCollection"/>, containing a nested group of clauses (most commonly because of order of
		/// operations and a different <see cref="Operator"/>);
		/// <para>* A <see cref="SphinxQuery"/>, containing actual information as to what this component of the original search query
		/// is looking for.</para>
		/// </para>
		/// This list does NOT need to be a homogenous collection, and often will not be.
		/// </summary>
		public IReadOnlyList<ISphinxClause> Queries => _queries;

		/// <summary>
		/// The operator that combines all of the <see cref="Queries"/>.
		/// </summary>
		public SphinxConnectiveOperator Operator { get; internal set; }

		#region IEnumerable<SphinxQuery>

		public IEnumerator<SphinxQuery> GetEnumerator()
		{
			return _queries.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion // IEnumerable<SphinxQuery>

		ImmutableList<ISphinxClause> _queries = ImmutableList<ISphinxClause>.Empty;
	}
}
