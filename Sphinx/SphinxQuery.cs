﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx
{
	public sealed class SphinxQuery
	{
		public IReadOnlyList<SphinxQueryString> ExcludedStrings => _excludedStrings;

		internal ParseOperationError AddExcludedString( SphinxQueryString str )
		{
			if ( str == null )
			{
				return new ParseOperationError( $"{nameof( str )} cannot be null to {nameof( AddExcludedString )}." );
			}

			_excludedStrings = _excludedStrings.Add( str );
			return null;
		}

		ImmutableList<SphinxQueryString> _excludedStrings = ImmutableList<SphinxQueryString>.Empty;
	}
}
