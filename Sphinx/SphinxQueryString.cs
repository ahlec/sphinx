﻿using System;

namespace Sphinx
{
	public sealed class SphinxQueryString
	{
		public String String { get; internal set; }

		public Boolean RequiresExactMatch { get; internal set; }
	}
}
