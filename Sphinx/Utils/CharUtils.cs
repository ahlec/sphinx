﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sphinx.Utils
{
	internal static class CharUtils
	{
		internal static Char EscapeCharacter = '\\';
		internal static Char ExcludedStringPrefix = '-';

		internal static Boolean IsOpeningQuotationMark( Char character )
		{
			return ( character == _asciiQuote || character == _smartQuoteOpen );
		}

		internal static Boolean IsClosingQuotationMark( Char character )
		{
			return ( character == _asciiQuote || character == _smartQuoteClose );
		}

		static Char _asciiQuote = '"';
		static Char _smartQuoteOpen = '“';
		static Char _smartQuoteClose = '”';
	}
}
